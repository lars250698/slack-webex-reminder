#!/usr/bin/env python
import os
import argparse
from slackclient import SlackClient

parser = argparse.ArgumentParser()
parser.add_argument('-c', '--channel', help='ID of the destination slack channel', required=True)
parser.add_argument('-w', '--webex', help='ID of the webex', required=True)
args = parser.parse_args()

slack_token = os.environ["SLACK_API_TOKEN"]
sc = SlackClient(slack_token)

sc.api_call(
        "chat.command",
        channel=args.channel,
        command="/webex",
        text=args.webex
)
