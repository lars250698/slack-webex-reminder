# Slack webex reminder

This is a script intended to be used as a cronjob to send webex reminders to slack channels.

## Installation

Install python and all dependencies (argparse and slackclient) with pip.

## Usage

Run the script like you would run any other script (duh). The following args are available:


* **-h, --help**: Show a help page
* **-c, --channel**: Specify the channel the reminder should be sent to. *Required*
* **-w, --webex**: Specify the webex ID. *Required*

## License

It's 20 lines of python, just do what the hell you want with this.
